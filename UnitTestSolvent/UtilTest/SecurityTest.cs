﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solvent.Util;

namespace UnitTestSolvent
{
    /// <summary>
    /// Test Class for Security
    /// </summary>
    [TestClass]
    public class UnitTestSecurity
    {
        /// <summary>
        /// Tests for encrypting password - note user name after User(ex:sSmith1234)
        /// </summary>
        [TestMethod]
        public void Test_Encrypt_Should_Return_Different_Value()
        {
          
            var encrypted = Security.Encrypt("test");
          
            Assert.AreNotEqual("test", encrypted);

        }

        [TestMethod]
        public void Test_Encrypt_And_Decrypt_Should_Return_Same_Value()
        {

            var encrypted = Security.Encrypt("test");
            var decrypted = Security.Decrypt(encrypted);
            Assert.AreEqual("test", decrypted);

        }

       
    }
}
