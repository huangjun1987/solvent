﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestSolvent.Controller
{
    [TestClass]
    public class TransactionControllerTest
    {
        private Solvent.Controller.TransactionController transactionController;

        [TestInitialize]
        public void TestInitialize()
        {
            this.transactionController = new Solvent.Controller.TransactionController();
        }

        [TestMethod]
        public void TestToRetrieveCategoryTypes()
        {

            List<Solvent.Model.CategoryType> categoryTypes = this.transactionController.GetCategoryTypes();

            Assert.IsTrue(categoryTypes.Count > 0);
        }

        [TestMethod]
        public void TestToGetTransactionsByUserID()
        {
            List<Solvent.Model.Transaction> validTransactions = this.transactionController.GetTransactionsByUserID(1000);

            Assert.IsTrue(validTransactions.Count > 0);

            List<Solvent.Model.Transaction> inValidTransactions = this.transactionController.GetTransactionsByUserID(9999);

            Assert.IsFalse(inValidTransactions.Count > 0);
        }

        [TestMethod]
        public void TestToAddTransaction()
        {
            Solvent.Model.Transaction validTransaction = new Solvent.Model.Transaction
            {
                IsIncome = 1,
                Amount = 400,
                Date = DateTime.Now,
                CategoryId = 1000,
                AccountId = 1005
            };

            int transactionId = this.transactionController.AddTransaction(validTransaction);
            Assert.IsTrue(transactionId > 0);

            Solvent.Model.Transaction inValidTransaction = new Solvent.Model.Transaction();
            validTransaction.IsIncome = 1;
            validTransaction.Amount = 400;
            validTransaction.Date = DateTime.Now;
            validTransaction.CategoryId = 1004;
            validTransaction.AccountId = 9999;

            try { 
                this.transactionController.AddTransaction(inValidTransaction);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                ex.ToString();
                Console.WriteLine("Working as expected");
            }

        }

        [TestCleanup]
        public void TestCleanup()
        {
            this.transactionController = null;
        }
    }
}
