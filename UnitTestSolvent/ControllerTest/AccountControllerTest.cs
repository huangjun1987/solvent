﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestSolvent.Controller
{
    [TestClass]
    public class AccountControllerTest
    {
        private Solvent.Controller.AccountController accountController;

        [TestInitialize]
        public void TestInitialize()
        {
            this.accountController = new Solvent.Controller.AccountController();
        }

        [TestMethod]
        public void TestToRetrieveAccountsByUserId()
        {
            List<Solvent.Model.AccountDTO> accounts = this.accountController.GetAccountsByUserID(1000);

            Assert.IsTrue(accounts.Count > 0);

            List<Solvent.Model.AccountDTO> noaccounts = this.accountController.GetAccountsByUserID(9999);

            Assert.IsFalse(noaccounts.Count > 0);
        }

        [TestMethod]
        public void TestToAddAccount()
        {
            Solvent.Model.AccountDTO validAccount = new Solvent.Model.AccountDTO
            {
                AccountTypeID = 1000,
                UserID = 1000,
                TraditionalOrRoth = "Traditional",
                IsAsset = true,
                Owner = "Self",
                Balance = 1001
            };

            int accountId = this.accountController.AddAccount(validAccount);
            Assert.IsTrue(accountId > 0);

            Solvent.Model.AccountDTO inValidAccount = new Solvent.Model.AccountDTO();
            validAccount.AccountTypeID = 9999;
            validAccount.UserID = 9999;
            validAccount.TraditionalOrRoth = "Traditional";
            validAccount.IsAsset = true;
            validAccount.Owner = "Self";
            validAccount.Balance = 1001;

            try
            {
                this.accountController.AddAccount(inValidAccount);
                // Above line should fail and the below assert should not get executed
                Assert.Fail();
            }
            catch(Exception ex)
            {
                ex.ToString();
                Console.WriteLine("Working as expected");
            }

        }

        [TestMethod]
        public void TestToRetrieveAccountTypes()
        {

            List<Solvent.Model.AccountTypeDTO> accountTypes = this.accountController.GetAccountTypes();

            Assert.IsTrue(accountTypes.Count > 0);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this.accountController = null;
        }
    }
}
