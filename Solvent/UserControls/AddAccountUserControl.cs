﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Solvent.Util;
using Solvent.Controller;
using Solvent.View;

namespace Solvent.UserControls
{
    /// <summary>
    /// This class models a user control to add an account.
    /// </summary>
    public partial class AddAccountUserControl : UserControl
    {
        private readonly AccountController accountController;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public AddAccountUserControl()
        {
            InitializeComponent();
            this.accountController = new AccountController();
            PopulateComboBox();
        }

        /// <summary>
        /// This method populates the comboboxes on the form.
        /// </summary>
        private void PopulateComboBox()
        {
            ComboBoxUtil.UpdateRothTraditionalComboBox(this.isTraditionalComboBox);
            this.isTraditionalComboBox.SelectedIndex = 0;
            ComboBoxUtil.UpdateAssetComboBox(this.AssetLiabilityComboBox);
            ComboBoxUtil.UpdateOwnerComboBox(this.OwnerComboBox);

            try
            {
                List <Solvent.Model.AccountTypeDTO> accountTypes = this.accountController.GetAccountTypes();

                foreach (var item in accountTypes)
                {
                    this.AccountTypeComboBox.Items.Add(new { Text = item.AccountTypeName, Value = item.AccountTypeID });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while fetching account Types!!!! - " + ex.Message,
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// This method handles when the clear button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            this.ClearAll();
        }

        /// <summary>
        /// This method clears the form.
        /// </summary>
        private void ClearAll()
        {
            this.CurrentBalanceTextBox.Text = "";
            this.isTraditionalComboBox.SelectedIndex = 0;
            this.AssetLiabilityComboBox.SelectedIndex = -1;
            this.OwnerComboBox.SelectedIndex = -1;
            this.AccountTypeComboBox.SelectedIndex = -1;
        }

        /// <summary>
        /// This method handles when the add account button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddAccountButton_Click(object sender, EventArgs e)
        {

            // Validation
            if (this.AccountTypeComboBox.SelectedItem == null)
            {
                MessageBox.Show("Account Type is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.isTraditionalComboBox.SelectedItem == null)
            {
                MessageBox.Show("Is Traditional selection is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.OwnerComboBox.SelectedItem == null)
            {
                MessageBox.Show("Owner is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.AssetLiabilityComboBox.SelectedItem == null)
            {
                MessageBox.Show("Asset or Liability selection is required!!!! - ",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var accountType = (this.AccountTypeComboBox.SelectedItem as dynamic).Value;
            var isTraditional = this.isTraditionalComboBox.SelectedItem.ToString();
            var assetOrLiability = this.AssetLiabilityComboBox.SelectedItem.ToString();
            var owner = this.OwnerComboBox.SelectedItem.ToString();
            var balanceText = this.CurrentBalanceTextBox.Text;

            int balance;

            try
            {
                balance = Int32.Parse(balanceText);
                if (balance < 0)
                {
                    throw new Exception("Invalid Number");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("Balance should be a positive number!!!!",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Solvent.Model.AccountDTO account = new Solvent.Model.AccountDTO
            {
                AccountTypeID = accountType,
                IsAsset = assetOrLiability == "Asset",
                TraditionalOrRoth = isTraditional,
                Owner = owner,
                Balance = balance,
                UserID = MainDashboard.UserID
            };

            try
            {

                int accountId = this.accountController.AddAccount(account);

                MessageBox.Show("Account has been successfully created. Your account Id is - " + accountId,
                   "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.ClearAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while creating Account. Please contact support!!!! - " + ex.Message,
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
