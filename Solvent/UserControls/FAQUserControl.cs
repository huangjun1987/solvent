﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solvent.Model;

namespace Solvent.UserControls
{
    public partial class FAQUserControl : UserControl
    {
        private List<FAQDTO> FAQDTOs;
        public FAQUserControl()
        {
            InitializeComponent();
            FAQDTOs = new List<FAQDTO>();
            FAQDTOs.Add(new FAQDTO
            {
                Question= "Sample Question1 about Income;",
                Answer = "Sample answer1 to restart computer."
            });
            FAQDTOs.Add(new FAQDTO
            {
                Question = "Sample Question1 about Expense;",
                Answer = "Sample answer1 to call Chuck."
            });
            LoadFAQ();
        }

        private void LoadFAQ()
        {
            richTextBoxFAQ.Clear();
            var filteredFAQDTOs = new List<FAQDTO>();
            if (string.IsNullOrWhiteSpace(textBoxKeyword.Text))
            {
                filteredFAQDTOs = FAQDTOs;
            }
            else
            {
                string keyword = textBoxKeyword.Text.Trim().ToLower();
                filteredFAQDTOs = FAQDTOs.Where(f => f.Question.ToLower().Contains(keyword) || f.Answer.ToLower().Contains(keyword)).ToList();
            }
            foreach (var faq in filteredFAQDTOs)
            {
                richTextBoxFAQ.SelectionFont = new Font(richTextBoxFAQ.SelectionFont, FontStyle.Bold);
                richTextBoxFAQ.AppendText(faq.Question + Environment.NewLine);
                richTextBoxFAQ.SelectionFont = new Font(richTextBoxFAQ.SelectionFont, FontStyle.Regular);
                richTextBoxFAQ.AppendText(faq.Answer + Environment.NewLine + Environment.NewLine);
            }         

            this.richTextBoxFAQ.ReadOnly = true;
            this.richTextBoxFAQ.BackColor = Color.White;
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {           
            LoadFAQ();
        }
    }
}
