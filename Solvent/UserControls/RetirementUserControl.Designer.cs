﻿namespace Solvent.UserControls
{
    partial class RetirementUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spouseContributionLabel = new System.Windows.Forms.Label();
            this.spouseContributionNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.spouseCurrentIncomeTextBox = new System.Windows.Forms.TextBox();
            this.spouseCurrentIncomeLabel = new System.Windows.Forms.Label();
            this.contributionLabel = new System.Windows.Forms.Label();
            this.contributionNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.currentIncomeTextBox = new System.Windows.Forms.TextBox();
            this.currentIncomeLabel = new System.Windows.Forms.Label();
            this.retirementIncomeTextBox = new System.Windows.Forms.TextBox();
            this.retirementIncomeLabel = new System.Windows.Forms.Label();
            this.retirementAgeTextBox = new System.Windows.Forms.TextBox();
            this.retirementAgeLabel = new System.Windows.Forms.Label();
            this.spouseDateOfBirthLabel = new System.Windows.Forms.Label();
            this.spouseDateOfBirthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateOfBirthLabel = new System.Windows.Forms.Label();
            this.dateOfBirthDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.interestLabel = new System.Windows.Forms.Label();
            this.interestNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.retirementAccountListView = new System.Windows.Forms.ListView();
            this.accountID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.accountTypeName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.owner = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.balance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveUpdatesButton = new System.Windows.Forms.Button();
            this.estimatedRetirementIncomeLabel = new System.Windows.Forms.Label();
            this.estimatedRetirementIncomeTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spouseContributionNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contributionNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // spouseContributionLabel
            // 
            this.spouseContributionLabel.AutoSize = true;
            this.spouseContributionLabel.Location = new System.Drawing.Point(299, 96);
            this.spouseContributionLabel.Name = "spouseContributionLabel";
            this.spouseContributionLabel.Size = new System.Drawing.Size(143, 13);
            this.spouseContributionLabel.TabIndex = 31;
            this.spouseContributionLabel.Text = "Spouse % 401k Contribution:";
            // 
            // spouseContributionNumericUpDown
            // 
            this.spouseContributionNumericUpDown.DecimalPlaces = 1;
            this.spouseContributionNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spouseContributionNumericUpDown.Location = new System.Drawing.Point(466, 94);
            this.spouseContributionNumericUpDown.Name = "spouseContributionNumericUpDown";
            this.spouseContributionNumericUpDown.Size = new System.Drawing.Size(147, 20);
            this.spouseContributionNumericUpDown.TabIndex = 7;
            // 
            // spouseCurrentIncomeTextBox
            // 
            this.spouseCurrentIncomeTextBox.Location = new System.Drawing.Point(466, 54);
            this.spouseCurrentIncomeTextBox.Name = "spouseCurrentIncomeTextBox";
            this.spouseCurrentIncomeTextBox.Size = new System.Drawing.Size(147, 20);
            this.spouseCurrentIncomeTextBox.TabIndex = 6;
            // 
            // spouseCurrentIncomeLabel
            // 
            this.spouseCurrentIncomeLabel.AutoSize = true;
            this.spouseCurrentIncomeLabel.Location = new System.Drawing.Point(299, 57);
            this.spouseCurrentIncomeLabel.Name = "spouseCurrentIncomeLabel";
            this.spouseCurrentIncomeLabel.Size = new System.Drawing.Size(157, 13);
            this.spouseCurrentIncomeLabel.TabIndex = 28;
            this.spouseCurrentIncomeLabel.Text = "Spouse Current Annual Income:";
            // 
            // contributionLabel
            // 
            this.contributionLabel.AutoSize = true;
            this.contributionLabel.Location = new System.Drawing.Point(15, 96);
            this.contributionLabel.Name = "contributionLabel";
            this.contributionLabel.Size = new System.Drawing.Size(104, 13);
            this.contributionLabel.TabIndex = 27;
            this.contributionLabel.Text = "% 401k Contribution:";
            // 
            // contributionNumericUpDown
            // 
            this.contributionNumericUpDown.DecimalPlaces = 1;
            this.contributionNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.contributionNumericUpDown.Location = new System.Drawing.Point(143, 94);
            this.contributionNumericUpDown.Name = "contributionNumericUpDown";
            this.contributionNumericUpDown.Size = new System.Drawing.Size(147, 20);
            this.contributionNumericUpDown.TabIndex = 2;
            // 
            // currentIncomeTextBox
            // 
            this.currentIncomeTextBox.Location = new System.Drawing.Point(143, 54);
            this.currentIncomeTextBox.Name = "currentIncomeTextBox";
            this.currentIncomeTextBox.Size = new System.Drawing.Size(147, 20);
            this.currentIncomeTextBox.TabIndex = 1;
            // 
            // currentIncomeLabel
            // 
            this.currentIncomeLabel.AutoSize = true;
            this.currentIncomeLabel.Location = new System.Drawing.Point(15, 57);
            this.currentIncomeLabel.Name = "currentIncomeLabel";
            this.currentIncomeLabel.Size = new System.Drawing.Size(118, 13);
            this.currentIncomeLabel.TabIndex = 24;
            this.currentIncomeLabel.Text = "Current Annual Income:";
            // 
            // retirementIncomeTextBox
            // 
            this.retirementIncomeTextBox.Location = new System.Drawing.Point(143, 171);
            this.retirementIncomeTextBox.Name = "retirementIncomeTextBox";
            this.retirementIncomeTextBox.Size = new System.Drawing.Size(147, 20);
            this.retirementIncomeTextBox.TabIndex = 4;
            // 
            // retirementIncomeLabel
            // 
            this.retirementIncomeLabel.Location = new System.Drawing.Point(15, 167);
            this.retirementIncomeLabel.Name = "retirementIncomeLabel";
            this.retirementIncomeLabel.Size = new System.Drawing.Size(122, 29);
            this.retirementIncomeLabel.TabIndex = 22;
            this.retirementIncomeLabel.Text = "Desired Annual Retirement Income:";
            // 
            // retirementAgeTextBox
            // 
            this.retirementAgeTextBox.Location = new System.Drawing.Point(143, 132);
            this.retirementAgeTextBox.Name = "retirementAgeTextBox";
            this.retirementAgeTextBox.Size = new System.Drawing.Size(147, 20);
            this.retirementAgeTextBox.TabIndex = 3;
            // 
            // retirementAgeLabel
            // 
            this.retirementAgeLabel.AutoSize = true;
            this.retirementAgeLabel.Location = new System.Drawing.Point(15, 135);
            this.retirementAgeLabel.Name = "retirementAgeLabel";
            this.retirementAgeLabel.Size = new System.Drawing.Size(122, 13);
            this.retirementAgeLabel.TabIndex = 20;
            this.retirementAgeLabel.Text = "Desired Retirement Age:";
            // 
            // spouseDateOfBirthLabel
            // 
            this.spouseDateOfBirthLabel.AutoSize = true;
            this.spouseDateOfBirthLabel.Location = new System.Drawing.Point(299, 22);
            this.spouseDateOfBirthLabel.Name = "spouseDateOfBirthLabel";
            this.spouseDateOfBirthLabel.Size = new System.Drawing.Size(108, 13);
            this.spouseDateOfBirthLabel.TabIndex = 19;
            this.spouseDateOfBirthLabel.Text = "Spouse Date of Birth:";
            // 
            // spouseDateOfBirthDateTimePicker
            // 
            this.spouseDateOfBirthDateTimePicker.Location = new System.Drawing.Point(413, 16);
            this.spouseDateOfBirthDateTimePicker.Name = "spouseDateOfBirthDateTimePicker";
            this.spouseDateOfBirthDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.spouseDateOfBirthDateTimePicker.TabIndex = 5;
            // 
            // dateOfBirthLabel
            // 
            this.dateOfBirthLabel.AutoSize = true;
            this.dateOfBirthLabel.Location = new System.Drawing.Point(15, 22);
            this.dateOfBirthLabel.Name = "dateOfBirthLabel";
            this.dateOfBirthLabel.Size = new System.Drawing.Size(69, 13);
            this.dateOfBirthLabel.TabIndex = 17;
            this.dateOfBirthLabel.Text = "Date of Birth:";
            // 
            // dateOfBirthDateTimePicker
            // 
            this.dateOfBirthDateTimePicker.Location = new System.Drawing.Point(90, 16);
            this.dateOfBirthDateTimePicker.Name = "dateOfBirthDateTimePicker";
            this.dateOfBirthDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateOfBirthDateTimePicker.TabIndex = 0;
            // 
            // interestLabel
            // 
            this.interestLabel.AutoSize = true;
            this.interestLabel.Location = new System.Drawing.Point(299, 174);
            this.interestLabel.Name = "interestLabel";
            this.interestLabel.Size = new System.Drawing.Size(156, 13);
            this.interestLabel.TabIndex = 33;
            this.interestLabel.Text = "Estimated Annual Interest Rate:";
            // 
            // interestNumericUpDown
            // 
            this.interestNumericUpDown.DecimalPlaces = 1;
            this.interestNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.interestNumericUpDown.Location = new System.Drawing.Point(466, 172);
            this.interestNumericUpDown.Name = "interestNumericUpDown";
            this.interestNumericUpDown.Size = new System.Drawing.Size(147, 20);
            this.interestNumericUpDown.TabIndex = 9;
            // 
            // retirementAccountListView
            // 
            this.retirementAccountListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.accountID,
            this.accountTypeName,
            this.tax,
            this.owner,
            this.balance});
            this.retirementAccountListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.retirementAccountListView.HideSelection = false;
            this.retirementAccountListView.Location = new System.Drawing.Point(18, 257);
            this.retirementAccountListView.Name = "retirementAccountListView";
            this.retirementAccountListView.Size = new System.Drawing.Size(595, 157);
            this.retirementAccountListView.TabIndex = 34;
            this.retirementAccountListView.TabStop = false;
            this.retirementAccountListView.UseCompatibleStateImageBehavior = false;
            this.retirementAccountListView.View = System.Windows.Forms.View.Details;
            // 
            // accountID
            // 
            this.accountID.Text = "Account ID";
            this.accountID.Width = 114;
            // 
            // accountTypeName
            // 
            this.accountTypeName.Text = "Type";
            this.accountTypeName.Width = 114;
            // 
            // tax
            // 
            this.tax.Text = "Tax";
            this.tax.Width = 114;
            // 
            // owner
            // 
            this.owner.Text = "Owner";
            this.owner.Width = 114;
            // 
            // balance
            // 
            this.balance.Text = "Balance";
            this.balance.Width = 114;
            // 
            // saveUpdatesButton
            // 
            this.saveUpdatesButton.Location = new System.Drawing.Point(466, 130);
            this.saveUpdatesButton.Name = "saveUpdatesButton";
            this.saveUpdatesButton.Size = new System.Drawing.Size(147, 23);
            this.saveUpdatesButton.TabIndex = 8;
            this.saveUpdatesButton.Text = "Save Updates";
            this.saveUpdatesButton.UseVisualStyleBackColor = true;
            this.saveUpdatesButton.Click += new System.EventHandler(this.SaveUpdatesButton_Click);
            // 
            // estimatedRetirementIncomeLabel
            // 
            this.estimatedRetirementIncomeLabel.AutoSize = true;
            this.estimatedRetirementIncomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.estimatedRetirementIncomeLabel.Location = new System.Drawing.Point(14, 214);
            this.estimatedRetirementIncomeLabel.Name = "estimatedRetirementIncomeLabel";
            this.estimatedRetirementIncomeLabel.Size = new System.Drawing.Size(279, 20);
            this.estimatedRetirementIncomeLabel.TabIndex = 36;
            this.estimatedRetirementIncomeLabel.Text = "Estimated Annual Retirement Income:";
            // 
            // estimatedRetirementIncomeTextBox
            // 
            this.estimatedRetirementIncomeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.estimatedRetirementIncomeTextBox.Location = new System.Drawing.Point(299, 211);
            this.estimatedRetirementIncomeTextBox.Name = "estimatedRetirementIncomeTextBox";
            this.estimatedRetirementIncomeTextBox.ReadOnly = true;
            this.estimatedRetirementIncomeTextBox.Size = new System.Drawing.Size(157, 26);
            this.estimatedRetirementIncomeTextBox.TabIndex = 37;
            this.estimatedRetirementIncomeTextBox.TabStop = false;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(466, 214);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(147, 23);
            this.calculateButton.TabIndex = 10;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // RetirementUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.estimatedRetirementIncomeTextBox);
            this.Controls.Add(this.estimatedRetirementIncomeLabel);
            this.Controls.Add(this.saveUpdatesButton);
            this.Controls.Add(this.retirementAccountListView);
            this.Controls.Add(this.interestLabel);
            this.Controls.Add(this.interestNumericUpDown);
            this.Controls.Add(this.spouseContributionLabel);
            this.Controls.Add(this.spouseContributionNumericUpDown);
            this.Controls.Add(this.spouseCurrentIncomeTextBox);
            this.Controls.Add(this.spouseCurrentIncomeLabel);
            this.Controls.Add(this.contributionLabel);
            this.Controls.Add(this.contributionNumericUpDown);
            this.Controls.Add(this.currentIncomeTextBox);
            this.Controls.Add(this.currentIncomeLabel);
            this.Controls.Add(this.retirementIncomeTextBox);
            this.Controls.Add(this.retirementIncomeLabel);
            this.Controls.Add(this.retirementAgeTextBox);
            this.Controls.Add(this.retirementAgeLabel);
            this.Controls.Add(this.spouseDateOfBirthLabel);
            this.Controls.Add(this.spouseDateOfBirthDateTimePicker);
            this.Controls.Add(this.dateOfBirthLabel);
            this.Controls.Add(this.dateOfBirthDateTimePicker);
            this.Name = "RetirementUserControl";
            this.Size = new System.Drawing.Size(631, 431);
            ((System.ComponentModel.ISupportInitialize)(this.spouseContributionNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contributionNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interestNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label spouseContributionLabel;
        private System.Windows.Forms.NumericUpDown spouseContributionNumericUpDown;
        private System.Windows.Forms.TextBox spouseCurrentIncomeTextBox;
        private System.Windows.Forms.Label spouseCurrentIncomeLabel;
        private System.Windows.Forms.Label contributionLabel;
        private System.Windows.Forms.NumericUpDown contributionNumericUpDown;
        private System.Windows.Forms.TextBox currentIncomeTextBox;
        private System.Windows.Forms.Label currentIncomeLabel;
        private System.Windows.Forms.TextBox retirementIncomeTextBox;
        private System.Windows.Forms.Label retirementIncomeLabel;
        private System.Windows.Forms.TextBox retirementAgeTextBox;
        private System.Windows.Forms.Label retirementAgeLabel;
        private System.Windows.Forms.Label spouseDateOfBirthLabel;
        private System.Windows.Forms.DateTimePicker spouseDateOfBirthDateTimePicker;
        private System.Windows.Forms.Label dateOfBirthLabel;
        private System.Windows.Forms.DateTimePicker dateOfBirthDateTimePicker;
        private System.Windows.Forms.Label interestLabel;
        private System.Windows.Forms.NumericUpDown interestNumericUpDown;
        private System.Windows.Forms.ListView retirementAccountListView;
        private System.Windows.Forms.Button saveUpdatesButton;
        private System.Windows.Forms.Label estimatedRetirementIncomeLabel;
        private System.Windows.Forms.TextBox estimatedRetirementIncomeTextBox;
        private System.Windows.Forms.ColumnHeader accountID;
        private System.Windows.Forms.ColumnHeader accountTypeName;
        private System.Windows.Forms.ColumnHeader tax;
        private System.Windows.Forms.ColumnHeader owner;
        private System.Windows.Forms.ColumnHeader balance;
        private System.Windows.Forms.Button calculateButton;
    }
}
