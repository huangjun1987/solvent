﻿namespace Solvent.UserControls
{
    partial class FAQUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBoxFAQ = new System.Windows.Forms.RichTextBox();
            this.textBoxKeyword = new System.Windows.Forms.TextBox();
            this.buttonFilter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 69);
            this.label1.TabIndex = 0;
            this.label1.Text = "FAQ";
            // 
            // richTextBoxFAQ
            // 
            this.richTextBoxFAQ.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxFAQ.Location = new System.Drawing.Point(36, 199);
            this.richTextBoxFAQ.Name = "richTextBoxFAQ";
            this.richTextBoxFAQ.Size = new System.Drawing.Size(1454, 529);
            this.richTextBoxFAQ.TabIndex = 1;
            this.richTextBoxFAQ.Text = "";
            // 
            // textBoxKeyword
            // 
            this.textBoxKeyword.Location = new System.Drawing.Point(47, 113);
            this.textBoxKeyword.Name = "textBoxKeyword";
            this.textBoxKeyword.Size = new System.Drawing.Size(199, 38);
            this.textBoxKeyword.TabIndex = 2;
            // 
            // buttonFilter
            // 
            this.buttonFilter.Location = new System.Drawing.Point(317, 104);
            this.buttonFilter.Name = "buttonFilter";
            this.buttonFilter.Size = new System.Drawing.Size(190, 47);
            this.buttonFilter.TabIndex = 3;
            this.buttonFilter.Text = "buttonFilter";
            this.buttonFilter.UseVisualStyleBackColor = true;
            this.buttonFilter.Click += new System.EventHandler(this.buttonFilter_Click);
            // 
            // FAQUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonFilter);
            this.Controls.Add(this.textBoxKeyword);
            this.Controls.Add(this.richTextBoxFAQ);
            this.Controls.Add(this.label1);
            this.Name = "FAQUserControl";
            this.Size = new System.Drawing.Size(1673, 763);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBoxFAQ;
        private System.Windows.Forms.TextBox textBoxKeyword;
        private System.Windows.Forms.Button buttonFilter;
    }
}
