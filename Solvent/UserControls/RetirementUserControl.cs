﻿using System;
using System.Windows.Forms;
using Solvent.Controller;
using Solvent.Model;
using Solvent.Util;

namespace Solvent.UserControls
{
    /// <summary>
    /// This class models a user control for the Retirement Tab Page.
    /// </summary>
    public partial class RetirementUserControl : UserControl
    {
        private readonly AccountController accountController;
        private readonly UserController userController;
        public string UserID;
        private SolventUser user;
        private decimal balanceSum;
        private bool isUpdated;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public RetirementUserControl()
        {
            InitializeComponent();
            this.accountController = new AccountController();
            this.userController = new UserController();
        }

        /// <summary>
        /// This method refreshes the accounts in the retirement account list view.
        /// </summary>
        public void PopulateUserDetails()
        {
            try
            {
                this.user = this.userController.GetUserByUserID(int.Parse(this.UserID));
                this.dateOfBirthDateTimePicker.Value = this.user.DOB;
                this.retirementAgeTextBox.Text = this.user.DesiredRetirementAge.ToString();
                this.retirementIncomeTextBox.Text = this.user.DesiredRetirementIncome.ToString("C2");
                this.currentIncomeTextBox.Text = this.user.AnnualIncome.ToString("C2");
                this.contributionNumericUpDown.Value = (decimal)this.user.UserContribution;
                this.spouseDateOfBirthDateTimePicker.Value = this.user.SpouseDOB;
                this.spouseCurrentIncomeTextBox.Text = this.user.SpouseAnnualIncome.ToString("C2");
                this.spouseContributionNumericUpDown.Value = (decimal)this.user.SpouseContribution;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something is wrong with the database connection. " + ex.Message,
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// This method refreshes the accounts in the retirement account list view.
        /// </summary>
        public void RefreshRetirementAccounts()
        {
            try
            {
                this.retirementAccountListView.Items.Clear();
                if (string.IsNullOrEmpty(this.UserID))
                {
                    return;
                }
                foreach (Solvent.Model.AccountDTO account in this.accountController.GetRetirementAccountsByUserID(int.Parse(this.UserID)))
                {
                    ListViewItem accountLVI = new ListViewItem(account.AccountID.ToString());
                    accountLVI.SubItems.Add(account.AccountTypeName.ToString());
                    accountLVI.SubItems.Add(account.TraditionalOrRoth.ToString());
                    accountLVI.SubItems.Add(account.Owner.ToString());
                    accountLVI.SubItems.Add(account.Balance.ToString("C2"));
                    this.retirementAccountListView.Items.Add(accountLVI);
                    this.balanceSum += account.Balance;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something is wrong with the database connection. " + ex.Message,
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// This helper method updates the user in the database via the controller.
        /// </summary>
        /// <returns></returns>
        private bool UpdateHelper()
        {
            try
            {
                SolventUser newUser = new SolventUser
                {
                    DOB = this.dateOfBirthDateTimePicker.Value,
                    AnnualIncome = decimal.Parse(this.currentIncomeTextBox.Text.Replace("$", "").Replace(",", "")),
                    UserContribution = this.contributionNumericUpDown.Value,
                    DesiredRetirementAge = int.Parse(this.retirementAgeTextBox.Text),
                    DesiredRetirementIncome = decimal.Parse(this.retirementIncomeTextBox.Text.Replace("$", "").Replace(",", "")),
                    SpouseDOB = this.spouseDateOfBirthDateTimePicker.Value,
                    SpouseAnnualIncome = decimal.Parse(this.spouseCurrentIncomeTextBox.Text.Replace("$", "").Replace(",", "")),
                    SpouseContribution = this.spouseContributionNumericUpDown.Value
                };
                if (Validator.ValidateUser(newUser))
                {
                    return false;
                }
                return this.userController.UpdateUser(this.user, newUser);
            }
            catch (FormatException ex)
            {
                ex.ToString();
                MessageBox.Show("Something is wrong with the input. Currency fields and Desired Retirement Age must be numeric.",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something is wrong with the input or database connection. " + ex.Message,
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// This method handles when the save updates button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveUpdatesButton_Click(object sender, EventArgs e)
        {
            this.isUpdated = this.UpdateHelper();
            if (this.isUpdated)
            {
                this.PopulateUserDetails();
                MessageBox.Show("Your details were successfully updated.",
                                "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.isUpdated = false;
        }

        /// <summary>
        /// This method handles when the calculate button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalculateButton_Click(object sender, EventArgs e)
        {
            if (this.interestNumericUpDown.Value == 0)
            {
                MessageBox.Show("Select an estimated interest rate above 0.0%.",
                                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.UpdateHelper();
            this.PopulateUserDetails();
            this.estimatedRetirementIncomeTextBox.Text = this.userController.CalculateAnnualRetirementIncome(this.user, this.balanceSum, this.interestNumericUpDown.Value / 100.00000m).ToString("C2");
        }
    }
}
