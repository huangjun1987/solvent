﻿namespace Solvent.UserControls
{
    partial class AddTransactionUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransactionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.typeLabel = new System.Windows.Forms.Label();
            this.TransactionDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.CategoryComboBox = new System.Windows.Forms.ComboBox();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.AccountComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AmountTextBox = new System.Windows.Forms.TextBox();
            this.AmountLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.AddTransactionButton = new System.Windows.Forms.Button();
            this.checkBoxRecurring = new System.Windows.Forms.CheckBox();
            this.checkBoxNoEndingDate = new System.Windows.Forms.CheckBox();
            this.groupBoxNumOfRecurrances = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNumOfRecurrances = new System.Windows.Forms.TextBox();
            this.groupBoxIsRecurringTransaction = new System.Windows.Forms.GroupBox();
            this.groupBoxNumOfRecurrances.SuspendLayout();
            this.groupBoxIsRecurringTransaction.SuspendLayout();
            this.SuspendLayout();
            // 
            // TransactionTypeComboBox
            // 
            this.TransactionTypeComboBox.DisplayMember = "Text";
            this.TransactionTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransactionTypeComboBox.FormattingEnabled = true;
            this.TransactionTypeComboBox.Location = new System.Drawing.Point(491, 107);
            this.TransactionTypeComboBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.TransactionTypeComboBox.Name = "TransactionTypeComboBox";
            this.TransactionTypeComboBox.Size = new System.Drawing.Size(492, 39);
            this.TransactionTypeComboBox.TabIndex = 11;
            this.TransactionTypeComboBox.ValueMember = "Value";
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Location = new System.Drawing.Point(203, 114);
            this.typeLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(86, 32);
            this.typeLabel.TabIndex = 12;
            this.typeLabel.Text = "Type:";
            // 
            // TransactionDateTimePicker
            // 
            this.TransactionDateTimePicker.Location = new System.Drawing.Point(491, 229);
            this.TransactionDateTimePicker.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.TransactionDateTimePicker.Name = "TransactionDateTimePicker";
            this.TransactionDateTimePicker.Size = new System.Drawing.Size(492, 38);
            this.TransactionDateTimePicker.TabIndex = 13;
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(203, 243);
            this.dateLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(239, 32);
            this.dateLabel.TabIndex = 14;
            this.dateLabel.Text = "Transaction Date:";
            // 
            // CategoryComboBox
            // 
            this.CategoryComboBox.DisplayMember = "Text";
            this.CategoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CategoryComboBox.FormattingEnabled = true;
            this.CategoryComboBox.Location = new System.Drawing.Point(491, 343);
            this.CategoryComboBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.CategoryComboBox.Name = "CategoryComboBox";
            this.CategoryComboBox.Size = new System.Drawing.Size(492, 39);
            this.CategoryComboBox.TabIndex = 15;
            this.CategoryComboBox.ValueMember = "Value";
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(203, 351);
            this.categoryLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(138, 32);
            this.categoryLabel.TabIndex = 16;
            this.categoryLabel.Text = "Category:";
            // 
            // AccountComboBox
            // 
            this.AccountComboBox.DisplayMember = "Text";
            this.AccountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AccountComboBox.FormattingEnabled = true;
            this.AccountComboBox.Location = new System.Drawing.Point(491, 460);
            this.AccountComboBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.AccountComboBox.Name = "AccountComboBox";
            this.AccountComboBox.Size = new System.Drawing.Size(492, 39);
            this.AccountComboBox.TabIndex = 17;
            this.AccountComboBox.ValueMember = "Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 467);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 32);
            this.label2.TabIndex = 18;
            this.label2.Text = "Account:";
            // 
            // AmountTextBox
            // 
            this.AmountTextBox.Location = new System.Drawing.Point(491, 577);
            this.AmountTextBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.AmountTextBox.MaxLength = 50;
            this.AmountTextBox.Name = "AmountTextBox";
            this.AmountTextBox.Size = new System.Drawing.Size(492, 38);
            this.AmountTextBox.TabIndex = 21;
            // 
            // AmountLabel
            // 
            this.AmountLabel.AutoSize = true;
            this.AmountLabel.Location = new System.Drawing.Point(203, 584);
            this.AmountLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.AmountLabel.Name = "AmountLabel";
            this.AmountLabel.Size = new System.Drawing.Size(121, 32);
            this.AmountLabel.TabIndex = 20;
            this.AmountLabel.Text = "Amount:";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(763, 997);
            this.clearButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(187, 55);
            this.clearButton.TabIndex = 23;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // AddTransactionButton
            // 
            this.AddTransactionButton.Location = new System.Drawing.Point(365, 997);
            this.AddTransactionButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.AddTransactionButton.Name = "AddTransactionButton";
            this.AddTransactionButton.Size = new System.Drawing.Size(296, 55);
            this.AddTransactionButton.TabIndex = 22;
            this.AddTransactionButton.Text = "Add Transaction";
            this.AddTransactionButton.UseVisualStyleBackColor = true;
            this.AddTransactionButton.Click += new System.EventHandler(this.AddTransactionButton_Click);
            // 
            // checkBoxRecurring
            // 
            this.checkBoxRecurring.AutoSize = true;
            this.checkBoxRecurring.Location = new System.Drawing.Point(209, 668);
            this.checkBoxRecurring.Name = "checkBoxRecurring";
            this.checkBoxRecurring.Size = new System.Drawing.Size(332, 36);
            this.checkBoxRecurring.TabIndex = 25;
            this.checkBoxRecurring.Text = "Recurring Transaction";
            this.checkBoxRecurring.UseVisualStyleBackColor = true;
            this.checkBoxRecurring.CheckedChanged += new System.EventHandler(this.checkBoxRecurring_CheckedChanged);
            // 
            // checkBoxNoEndingDate
            // 
            this.checkBoxNoEndingDate.AutoSize = true;
            this.checkBoxNoEndingDate.Location = new System.Drawing.Point(9, 47);
            this.checkBoxNoEndingDate.Name = "checkBoxNoEndingDate";
            this.checkBoxNoEndingDate.Size = new System.Drawing.Size(253, 36);
            this.checkBoxNoEndingDate.TabIndex = 26;
            this.checkBoxNoEndingDate.Text = "No Ending Date";
            this.checkBoxNoEndingDate.UseVisualStyleBackColor = true;
            this.checkBoxNoEndingDate.CheckedChanged += new System.EventHandler(this.checkBoxNoEndingDate_CheckedChanged);
            // 
            // groupBoxNumOfRecurrances
            // 
            this.groupBoxNumOfRecurrances.Controls.Add(this.textBoxNumOfRecurrances);
            this.groupBoxNumOfRecurrances.Controls.Add(this.label1);
            this.groupBoxNumOfRecurrances.Location = new System.Drawing.Point(321, 34);
            this.groupBoxNumOfRecurrances.Name = "groupBoxNumOfRecurrances";
            this.groupBoxNumOfRecurrances.Size = new System.Drawing.Size(703, 112);
            this.groupBoxNumOfRecurrances.TabIndex = 27;
            this.groupBoxNumOfRecurrances.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number of Recurrances";
            // 
            // textBoxNumOfRecurrances
            // 
            this.textBoxNumOfRecurrances.Location = new System.Drawing.Point(435, 30);
            this.textBoxNumOfRecurrances.Name = "textBoxNumOfRecurrances";
            this.textBoxNumOfRecurrances.Size = new System.Drawing.Size(100, 38);
            this.textBoxNumOfRecurrances.TabIndex = 1;
            // 
            // groupBoxIsRecurringTransaction
            // 
            this.groupBoxIsRecurringTransaction.Controls.Add(this.checkBoxNoEndingDate);
            this.groupBoxIsRecurringTransaction.Controls.Add(this.groupBoxNumOfRecurrances);
            this.groupBoxIsRecurringTransaction.Location = new System.Drawing.Point(200, 711);
            this.groupBoxIsRecurringTransaction.Name = "groupBoxIsRecurringTransaction";
            this.groupBoxIsRecurringTransaction.Size = new System.Drawing.Size(1096, 182);
            this.groupBoxIsRecurringTransaction.TabIndex = 28;
            this.groupBoxIsRecurringTransaction.TabStop = false;
            this.groupBoxIsRecurringTransaction.Visible = false;
            // 
            // AddTransactionUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxIsRecurringTransaction);
            this.Controls.Add(this.checkBoxRecurring);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.AddTransactionButton);
            this.Controls.Add(this.AmountTextBox);
            this.Controls.Add(this.AmountLabel);
            this.Controls.Add(this.AccountComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CategoryComboBox);
            this.Controls.Add(this.categoryLabel);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.TransactionDateTimePicker);
            this.Controls.Add(this.TransactionTypeComboBox);
            this.Controls.Add(this.typeLabel);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "AddTransactionUserControl";
            this.Size = new System.Drawing.Size(1517, 1066);
            this.groupBoxNumOfRecurrances.ResumeLayout(false);
            this.groupBoxNumOfRecurrances.PerformLayout();
            this.groupBoxIsRecurringTransaction.ResumeLayout(false);
            this.groupBoxIsRecurringTransaction.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox TransactionTypeComboBox;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.DateTimePicker TransactionDateTimePicker;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.ComboBox CategoryComboBox;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ComboBox AccountComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox AmountTextBox;
        private System.Windows.Forms.Label AmountLabel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button AddTransactionButton;
        private System.Windows.Forms.CheckBox checkBoxRecurring;
        private System.Windows.Forms.CheckBox checkBoxNoEndingDate;
        private System.Windows.Forms.GroupBox groupBoxNumOfRecurrances;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNumOfRecurrances;
        private System.Windows.Forms.GroupBox groupBoxIsRecurringTransaction;
    }
}
