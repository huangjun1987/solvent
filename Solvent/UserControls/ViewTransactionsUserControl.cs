﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Solvent.Controller;
using Solvent.View;

namespace Solvent.UserControls
{
    /// <summary>
    /// This class models a user control to view transactions.
    /// </summary>
    public partial class ViewTransactionsUserControl : UserControl
    {
        private readonly TransactionController transactionController;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public ViewTransactionsUserControl()
        {
            InitializeComponent();
            transactionController = new TransactionController();
            LoadGrid();
        }

        /// <summary>
        /// This method loads the transaction history into the list view.
        /// </summary>
        public void LoadGrid()
        {
            this.TransactionListView.Items.Clear();
            Console.WriteLine("Transaction is getting refreshed");
            try
            {
                List<Solvent.Model.Transaction> transactions = this.transactionController.GetTransactionsByUserID(MainDashboard.UserID);

                if (transactions != null)
                {
                    foreach (var item in transactions)
                    {
                        var lvi = new ListViewItem(new[] { item.IncomeOrExpense, item.Date.ToShortDateString(), item.CategoryName, item.AccountName, item.Amount.ToString() });
                        this.TransactionListView.Items.Add(lvi);
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error while fetching transactions from database!!!!",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //private void RefreshDataGrid()
        //{
            
        //    this.TransactionListView.Invalidate();
        //    try
        //    {
        //        this.TransactionListView.Refresh();
        //    }
        //    catch (Exception)
        //    {
        //        MessageBox.Show("Error while fetching data from database!!!!",
        //            "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }

        //}
    }
}
