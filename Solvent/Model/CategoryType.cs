﻿namespace Solvent.Model
{
    /// <summary>
    /// This class models a category type.
    /// </summary>
    public class CategoryType
    {
        /// <summary>
        /// Getters and setters for category type instance variables.
        /// </summary>
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
