﻿using System;

namespace Solvent.Model
{
    /// <summary>
    /// This class models a user.
    /// </summary>
   public  class SolventUser
    {
        /// <summary>
        /// Getters and setters for user instance variables.
        /// </summary>
        public int UserID { get; set; }
        public DateTime DOB { get; set; }
        public int DesiredRetirementAge { get; set; }
        public decimal DesiredRetirementIncome { get; set; }
        public decimal AnnualIncome { get; set; }
        public decimal UserContribution { get; set; }
        public DateTime SpouseDOB { get; set; }
        public decimal SpouseAnnualIncome { get; set; }
        public decimal SpouseContribution { get; set; }
    }
}
