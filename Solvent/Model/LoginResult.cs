﻿namespace Solvent.Model
{
    /// <summary>
    /// This class models a login result
    /// </summary>
    public class LoginResult
    {
        /// <summary>
        /// Getters and setters for login result instance variables.
        /// </summary>
        public int UserID { get; set; }
        public string Name { get; set; }
    }
}
