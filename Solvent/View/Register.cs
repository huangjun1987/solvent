﻿using Solvent.Controller;
using System;
using System.Windows.Forms;

namespace Solvent.View
{
    /// <summary>
    /// This class is the view for Register
    /// </summary>
    public partial class Register : Form
    {
        public Form PreviousForm = null;
        private readonly RegisterController registerController;

        /// <summary>
        /// This is the constructor
        /// </summary>
        /// <param name="form"></param>
        public Register(Form form)
        {
            InitializeComponent();
            this.PreviousForm = form;
            registerController = new RegisterController();
        }

        /// <summary>
        /// This method handles when the add user button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAddUser_Click(object sender, EventArgs e)
        {
            var username = textBoxUsername.Text.Trim();
            var password = textBoxPassword.Text.Trim();
            var name = textBoxName.Text.Trim();
            var user = registerController.AddUser(username, password, name);

            if (user != null)
            {
                this.Hide();
                PreviousForm.Show();
            }

        }

        /// <summary>
        /// This method handles when the register form closes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Register_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.PreviousForm.Close();
        }

        private void ButtonCancelRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.PreviousForm.Show();
        }
    }
}
