﻿using System;
using System.Windows.Forms;

namespace Solvent.View
{
    /// <summary>
    /// This class models the main dashboard.
    /// </summary>
    public partial class MainDashboard : Form
    {
        public Form PreviousForm = null;
        public static int UserID;

        /// <summary>
        /// Constructor method.
        /// </summary>
        /// <param name="username">The username of the logged in user.</param>
        /// <param name="userId">The ID of the logged in user.</param>
        /// <param name="form">The login form.</param>
        public MainDashboard(string username, int userId, Form form)
        {
            UserID = userId;
            InitializeComponent();
            currentUserLabel.Text = "You are logged in as " + username;
            currentUserIDLabel.Text = userId.ToString();
            this.PreviousForm = form;
        }

        /// <summary>
        /// This method handles the Main Dashboard closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.PreviousForm.Close();
        }

        /// <summary>
        /// This method handles entering the Retirement Tab Page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RetirementTabPage_Enter(object sender, EventArgs e)
        {
            this.retirementUserControl.UserID = this.currentUserIDLabel.Text;
            this.retirementUserControl.PopulateUserDetails();
            this.retirementUserControl.RefreshRetirementAccounts();
        }

        /// <summary>
        /// This method handles when the logout button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogoutButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            PreviousForm.Visible = true;

        }

        /// <summary>
        /// This method handles when the view transaction tab is entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewTransaction_Enter(object sender, System.EventArgs e)
        {
            this.ViewTransactionsUserControl.LoadGrid();
        }

        /// <summary>
        /// This method handles when the add transaction tab is entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddTransaction_Enter(object sender, System.EventArgs e)
        {
            this.addTransactionUserControl1.RefreshAccountsComboBox();
        }
    }
}
