﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Solvent.Util;

namespace Solvent.Controller
{
    /// <summary>
    /// This is the controller for registration
    /// </summary>
    public class RegisterController
    {
        /// <summary>
        /// This method adds a user to the database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public User AddUser(string username, string password, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name can't be Empty");
                return null;
            }
            if (String.IsNullOrEmpty(username))
            {
                MessageBox.Show("Username can't be Empty");
                return null;
            }
            var dbContext = new SolventEntities(true);
            if (dbContext.Users.Any(i => i.Username == username))
            {
                MessageBox.Show("username already exists");
                return null;
            }
            if (String.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password can't be Empty");
                return null;
            }

            var user = new User()
            {
                Name = name,
                Username = username,
                Password = Security.Encrypt(password),
                DOB = DateTime.Now,
                DesiredRetirementAge = 0,
                DesiredRetirementIncome = 0,
                AnnualIncome = 0,
                C401KContribution= 0,
                SpouseDOB = DateTime.Now,
                SpouseAnnualIncome = 0,
                Spouse401KContribution = 0

            };
            dbContext.Users.Add(user);
            dbContext.SaveChanges();
            MessageBox.Show("User Added");
            return user;


        }

        internal User GetUserByID2(int userID)
        {
            string selectStatement =
                    @"
SELECT *
  FROM [dbo].[User]
  where UserID = @userID
";
            using (SqlConnection connection = ConnectionUtil.GetConnection())
            {
                connection.Open();

                using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                {
                   selectCommand.Parameters.AddWithValue("@userID", userID);
                    using (SqlDataReader reader = selectCommand.ExecuteReader())
                    {
                        User user = null;

                        while (reader.Read())
                        {
                            user = new User();
                            user.Username = reader["Username"].ToString();
                            user.Password = reader["Password"].ToString();
                            user.Name = reader["Name"].ToString();
                            user.DOB = DateTime.Parse(reader["DOB"].ToString());
                            user.DesiredRetirementAge = int.Parse(reader["DesiredRetirementAge"].ToString());
                            user.DesiredRetirementIncome = decimal.Parse(reader["DesiredRetirementIncome"].ToString());
                            user.AnnualIncome = decimal.Parse(reader["AnnualIncome"].ToString());
                            user.C401KContribution = decimal.Parse(reader["401KContribution"].ToString());
                            user.SpouseDOB = string.IsNullOrWhiteSpace(reader["SpouseDOB"].ToString())? (DateTime?)null: DateTime.Parse(reader["SpouseDOB"].ToString());
                            user.SpouseAnnualIncome = string.IsNullOrWhiteSpace(reader["SpouseAnnualIncome"].ToString()) ? (decimal?)null : decimal.Parse(reader["SpouseAnnualIncome"].ToString());
                            user.Spouse401KContribution = string.IsNullOrWhiteSpace(reader["Spouse401KContribution"].ToString()) ? (decimal?)null : decimal.Parse(reader["Spouse401KContribution"].ToString());
                        }
                        
                        return user;
                    }
                }

            }
        }

        public User UpdateUser(int userID, string username, string password, string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name can't be Empty");
                return null;
            }
            if (String.IsNullOrEmpty(username))
            {
                MessageBox.Show("Username can't be Empty");
                return null;
            }
            var dbContext = new SolventEntities(true);
            var user = dbContext.Users.Where(i => i.UserID == userID).FirstOrDefault();
            if (username != user.Username && dbContext.Users.Any(i => i.Username == username))
            {
              
                MessageBox.Show("username already exists");
                return null;
            }
            if (String.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password can't be Empty");
                return null;
            }

                user.Name = name;
                user.Username = username;
                user.Password = Security.Encrypt(password);
         
            dbContext.SaveChanges();
            MessageBox.Show("User Updated");
            return user;
        }

        public User GetUserByID(int userID)
        {
            var dbContext = new SolventEntities(true);
            var user = dbContext.Users.Where(i => i.UserID == userID).FirstOrDefault();
            return user;
        }
    }
}
