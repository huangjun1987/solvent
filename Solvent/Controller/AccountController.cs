﻿using Solvent.DAL;
using System.Collections.Generic;

namespace Solvent.Controller
{
    /// <summary>
    /// Controller class to mediate between AccountDAL and View.
    /// </summary>
    public class AccountController
    {
        private readonly AccountDAL accountDataSource;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public AccountController()
        {
            this.accountDataSource = new AccountDAL();
        }

        /// <summary>
        /// This method returns the list of all accountid and corresponding type for the given user.
        /// </summary>
        /// <param name="userID">The user whose accounts will be returned.</param>
        /// <returns>The list of accounts for the given user.</returns>
        public List<Solvent.Model.AccountDTO> GetAccountsByUserID(int userID)
        {
            return this.accountDataSource.GetAccountsByUserID(userID);
        }

        /// <summary>
        /// This method returns the list of retirement accounts for the given user.
        /// </summary>
        /// <param name="userID">The user whose retirement accounts will be returned.</param>
        /// <returns>The list of retirement accounts for the given user.</returns>
        public List<Solvent.Model.AccountDTO> GetRetirementAccountsByUserID(int userID)
        {
            return this.accountDataSource.GetRetirementAccountsByUserID(userID);
        }

        /// <summary>
        /// This method returns all available account types.
        /// </summary>
        /// <returns>The list of account types.</returns>
        public List<Solvent.Model.AccountTypeDTO> GetAccountTypes()
        {
            return this.accountDataSource.GetAccountTypes();
        }

        /// <summary>
        /// Adds Account to user
        /// </summary>
        /// <param name="Account">Account Object</param>
        /// <returns>Account Id of the created account</returns>
        public int AddAccount(Solvent.Model.AccountDTO account)
        {
            return this.accountDataSource.AddAccount(account);
        }
    }
}
