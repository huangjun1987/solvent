﻿using Solvent.DAL;
using Solvent.Model;
using System;

namespace Solvent.Controller
{
    /// <summary>
    /// Controller class to mediate between UserDAL and View.
    /// </summary>
    public class UserController
    {
        private readonly UserDAL userDataSource;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public UserController()
        {
            this.userDataSource = new UserDAL();
        }

        /// <summary>
        /// This method returns the details for the given user.
        /// </summary>
        /// <param name="userID">The user whose details will be returned.</param>
        /// <returns>The details for the given user.</returns>
        public SolventUser GetUserByUserID(int userID)
        {
            return this.userDataSource.GetUserByUserID(userID);
        }

        /// <summary>
        /// This method updates the specified user in the database.
        /// </summary>
        /// <param name="oldUser">The user to be updated in the database.</param>
        /// <param name="newUser">The new details of the user.</param>
        public bool UpdateUser(SolventUser oldUser, SolventUser newUser)
        {
            return this.userDataSource.UpdateUser(oldUser, newUser);
        }

        /// <summary>
        /// This method calculates the estimated annual retirement income for the user. Life expectancy: y = 0.00005x^3 - 0.0041x^2 - 0.8818x + 82.114
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="rate">The estimated annual interest rate.</param>
        /// <returns></returns>
        public decimal CalculateAnnualRetirementIncome(SolventUser user, decimal currentBalance, decimal rate)
        {
            decimal onePlusRate = 1.00000m + rate;
            decimal yearsUntilRetirement = user.DOB.Year + user.DesiredRetirementAge - DateTime.Today.Year;
            decimal retirementValueOfPrincipal = currentBalance * (decimal)Math.Pow((double)onePlusRate, (double)yearsUntilRetirement);
            decimal retirementValueOfPayments = (user.AnnualIncome * user.UserContribution / 100.00000m + user.SpouseAnnualIncome * user.SpouseContribution / 100.00000m) * ((decimal)Math.Pow((double)onePlusRate, (double)yearsUntilRetirement) - 1) / rate;
            decimal lifeExpectancy = (decimal)(0.00005 * Math.Pow(DateTime.Today.Year - user.DOB.Year, 3.00000) - 0.0041 * Math.Pow(DateTime.Today.Year - user.DOB.Year, 2.00000) - 0.8818 * Math.Pow(DateTime.Today.Year - user.DOB.Year, 1.00000) + 82.114);
            decimal retirementValue = retirementValueOfPrincipal + retirementValueOfPayments;
            decimal annualRetirementIncome = retirementValue / ((1.00000m - (1.00000m / (decimal)Math.Pow((double)onePlusRate, (double)lifeExpectancy - (double)yearsUntilRetirement))) / rate);
            return annualRetirementIncome;
        }
    }
}
