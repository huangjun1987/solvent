﻿using Solvent.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Solvent.Controller
{
    /// <summary>
    /// Controller class to mediate between TransactionDAL and View.
    /// </summary>
    public class TransactionController
    {
        private readonly TransactionDAL transactionDataSource;

        /// <summary>
        /// Constructor method.
        /// </summary>
        public TransactionController()
        {
            this.transactionDataSource = new TransactionDAL();
        }

        /// <summary>
        /// This method returns all available category types.
        /// </summary>
        /// <returns>The list of category types.</returns>
        public List<Solvent.Model.CategoryType> GetCategoryTypes()
        {
            return this.transactionDataSource.GetCategoryTypes();
        }

        /// <summary>
        /// Adds a new transaction
        /// </summary>
        /// <param name="Transaction">Transaction Object</param>
        /// <returns>Transaction Id of the created transaction</returns>
        public int AddTransaction(Solvent.Model.Transaction transaction)
        {
            return this.transactionDataSource.AddTransaction(transaction);
        }

        /// <summary>
        /// This method returns the list of transactions for the given user.
        /// </summary>
        /// <param name="userID">The user whose transactions will be returned.</param>
        /// <returns>The list of transactions for the given user.</returns>
        public List<Solvent.Model.Transaction> GetTransactionsByUserID(int userID)
        {
            return this.transactionDataSource.GetTransactionsByUserID(userID);
        }

        public int AddRecurringTransaction(RecurringTransaction recurringTransaction)
        {
            var dbContext = new SolventEntities(true);
            dbContext.RecurringTransactions.Add(recurringTransaction);
            dbContext.SaveChanges();
            ProcessRecurringTransactions();
            return recurringTransaction.RecurringTransactionID;
        }

        public void ProcessRecurringTransactions()
        {
            var dbContext = new SolventEntities(true);
            var completedRecurringTransactionIds = new List<int>();
            foreach (var recurringTransaction in dbContext.RecurringTransactions)
            {
                var previousPaymentDate = DateTime.MinValue;

                if (dbContext.Transactions.Any(t=>t.RecurringTransactionID == recurringTransaction.RecurringTransactionID))
                {
                    previousPaymentDate = dbContext.Transactions.Where(t => t.RecurringTransactionID == recurringTransaction.RecurringTransactionID).Select(t => t.Date).Max();
                }
                var nextPaymentDate = new DateTime(Math.Max(recurringTransaction.StartDate.Ticks, previousPaymentDate.AddMonths(1).Ticks));
                var finalPaymentDate = recurringTransaction.NumOfOccurances == null ? DateTime.MaxValue : recurringTransaction.StartDate.AddMonths((int)recurringTransaction.NumOfOccurances-1);

                while (nextPaymentDate <= finalPaymentDate && nextPaymentDate <=DateTime.Now)
                {
                    var transaction = new Solvent.Model.Transaction()
                    {
                        AccountId = recurringTransaction.AccountID,
                        IsIncome = recurringTransaction.IsIncome ? 1 : 0,
                        Date = nextPaymentDate,
                        CategoryId = recurringTransaction.CategoryID,
                        RecurringTransactionId=recurringTransaction.RecurringTransactionID
                    };
                    AddTransaction(transaction);
                    nextPaymentDate = nextPaymentDate.AddMonths(1);
                }

                if (nextPaymentDate > finalPaymentDate)
                {
                    completedRecurringTransactionIds.Add(recurringTransaction.RecurringTransactionID);
                }
            }

            if (completedRecurringTransactionIds.Any())
            {
                var completedRecurringTransactions = dbContext.RecurringTransactions.Where(t => completedRecurringTransactionIds.Contains(t.RecurringTransactionID));
                dbContext.RecurringTransactions.RemoveRange(completedRecurringTransactions);
                dbContext.SaveChanges();
            }
        }
    }
}
