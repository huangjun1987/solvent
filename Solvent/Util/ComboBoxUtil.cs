﻿using System.Windows.Forms;

namespace Solvent.Util
{
    /// <summary>
    /// This class is used to populate comboboxes in the UI.
    /// </summary>
    class ComboBoxUtil
    {

        /// <summary>
        /// Populates Roth or Traditional Combobox
        /// </summary>
        public static void UpdateRothTraditionalComboBox(ComboBox comboBox)
        {
            comboBox.Items.Add("Not Applicable");
            comboBox.Items.Add("Traditional");
            comboBox.Items.Add("Roth");
        }

        /// <summary>
        /// Populates Owner Combobox
        /// </summary>
        public static void UpdateOwnerComboBox(ComboBox comboBox)
        {
            comboBox.Items.Add("Self");
            comboBox.Items.Add("Spouse");
        }

        /// <summary>
        /// Populates Asset Combobox
        /// </summary>
        public static void UpdateAssetComboBox(ComboBox comboBox)
        {
            comboBox.Items.Add("Asset");
            comboBox.Items.Add("Liability");
        }

        /// <summary>
        /// Populates TransactionType Combobox
        /// </summary>
        public static void UpdateTransactionTypeComboBox(ComboBox comboBox)
        {
            comboBox.Items.Add("Expense");
            comboBox.Items.Add("Income");
        }
    }
}
